from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from account.models import WalletForm, Wallet
from igaming.models import GameForm, Game

from .views_casino import make_bet, deposit_money

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()

    return render(request, 'signup.html', {'form': form})

def home(request):

    if request.user.is_authenticated():
        if request.method == 'POST':

            form = WalletForm(request.POST)
            if form.is_valid():
                try:
                    deposit_money(request.user.id, form.save(commit=False))
                except Exception as e:
                    print(e)

            form_game = GameForm(request.POST)
            if form_game.is_valid():
                try:
                    make_bet(request.user, form_game.save(commit=False))
                except Exception as e:
                    print(e)

        return render(request, 'casino.html',
            {'form': WalletForm(), 'form_game': GameForm(),
                'wallets' : get_wallets(request.user.id),
                    'games': reversed(get_games(request.user.id))})

    else:
        return redirect('login')

def get_wallets(id):
    return Wallet.objects.filter(user_id=id)

def get_games(id):
    return Game.objects.filter(user_id=id)
