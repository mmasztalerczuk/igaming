from django.contrib import admin

from .models import Casino_Settings

admin.site.register(Casino_Settings)

try:
    if len(Casino_Settings.objects.all()) == 0:
        Casino_Settings.objects.create()
except:
    pass
