from django.db import models
from django.forms import ModelForm
from django.contrib.auth.models import User

class Casino_Settings(models.Model):
    login_bonus = models.PositiveIntegerField(default=100)
    deposit_bonus = models.PositiveIntegerField(default=20)
    bet = models.PositiveIntegerField(default=2)
    wager_req = models.PositiveIntegerField(default=20)

class Game(models.Model):
    RED   = 'RED'
    BLACK = 'BLACK'
    ZERO  = 'ZERO'

    BET_CHOICES = (
        (RED, RED),
        (BLACK, BLACK),
        (ZERO, ZERO)
    )

    user = models.ForeignKey(User)

    result = models.CharField(
        max_length=5,
        choices=BET_CHOICES,
        default=ZERO
    )

    bet_color = models.CharField(
        max_length=5,
        choices=BET_CHOICES,
        default=ZERO
    )


class GameForm(ModelForm):
    class Meta:
        model = Game
        fields = ['bet_color']
