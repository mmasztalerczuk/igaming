import random
from django.contrib.auth.models import User
from igaming.models import GameForm, Game, Casino_Settings
from account.models import WalletForm, Wallet
from django.db.models import Max
from django.db import transaction
from django.db.models import Q



def return_color():
    """Simulation of spin

    Returns:
        None
    """

    bet = random.randint(0, 99)

    if bet == 0:
        return Game.ZERO

    if bet%2:
        return Game.RED
    else:
        return Game.BLACK

def change_wallet(id, wallet, game, settings):
    """This function is checking the status of bet and if bet is winning then
       money is added to wallet (or subtract the money from wallet)

    Args:
        id: id of user.
        wallet: wallet of user which will be used to add or remove money
        game: game which will be use to store the of result of bet
        settings: object with settting (size of bonus, etc)

    Returns:
        None
    """

    game.result = return_color()

    if game.bet_color == game.result:
        wallet.cash += settings.bet
    else:
        wallet.cash -= settings.bet

    wallet.save()
    game.user = id
    game.save()

@transaction.atomic
def make_bet(id, game):
    """ Entry function for new bet. At the beginning the function is checking
        all wallets except BNS (which is represents the wallet with bonus money).
        There should be only one 'real' wallet.
        If user has enough money on real wallet, then this wallet will be use.

        When user don't have enough real money, the function will be use one of
        bonus wallets.

        Args:
            id: id of user.
            game: game which will be use to store the of result of bet

        Returns:
            None

    """

    wallets = Wallet.objects.filter(user_id=id).exclude(currency=Wallet.BNS)
    settings = Casino_Settings.objects.get()

    for wallet in wallets: # should be one
        if wallet.cash >= settings.bet:
            change_wallet(id, wallet, game, settings)

            bonus_wallets = Wallet.objects.filter(user_id=id, currency=Wallet.BNS)
            for wallet in bonus_wallets:
                update_bonus_wallets(wallet, settings)
            return
    else:
        wallets = Wallet.objects.filter(user_id=id, currency=Wallet.BNS)

        for wallet in wallets:
            if wallet.cash >= settings.bet:
                change_wallet(id, wallet, game, settings)
                return

def update_bonus_wallets(wallet, settings):
    """ When user is spending money, every of his bonus wallet should be updated
        Also, this function is checking if bonus money should be transform on
        real wallet.

        Args:
            wallet: bonus wallet to check
            settings: configuration of igaming

        Returns:
            None
    """

    if wallet.cash != 0:
        wallet.spend_money += settings.bet
        if (wallet.wager_req * wallet.cash) + \
                 (wallet.wager_req * wallet.cents)/100 <= wallet.spend_money:

            user = User.objects.get(id=wallet.user_id)
            real_wallet = Wallet.objects.get(user=wallet.user, currency=Wallet.EUR)
            real_wallet.cash += wallet.cash
            real_wallet.cents += wallet.cents

            wallet.cents = 0
            wallet.cash  = 0

            real_wallet.save()
        wallet.save()



@transaction.atomic
def deposit_money(id, wallet):
    """ Adding real money on user account.

        Args:
            id: id of user
            wallet: real wallet where money will be stored

        Returns:
            None
    """
    if wallet.currency == Wallet.EUR:
        obj = Wallet.objects.get(user_id=id, currency=wallet.currency)
        tmp = obj.cents + wallet.cents

        obj.cash += wallet.cash + tmp/100
        obj.cents = tmp%100

        obj.save()
