from django.apps import AppConfig
from .models import Casino_Setting

class IgamingConfig(AppConfig):
    name = 'igaming'
