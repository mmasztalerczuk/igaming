from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.core.validators import MaxValueValidator

class Wallet(models.Model):
    USD = 'USD'
    EUR = 'EUR'
    PLN = 'PLN'
    BNS = 'BNS'

    CURRENCY_CHOICES = (
        (EUR, 'Euro'),
        (BNS, 'Bonus')
    )

    user = models.ForeignKey(User)
    cash = models.PositiveIntegerField(default=0)
    cents = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(99)])
    wager_req = models.PositiveIntegerField(default=0)
    spend_money = models.PositiveIntegerField(default=0)

    currency = models.CharField(
        max_length=3,
        choices=CURRENCY_CHOICES,
        default=EUR,
    )

class WalletForm(ModelForm):
    class Meta:
        model = Wallet
        fields = ['cash', 'cents', 'currency']
