from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.contrib.auth.signals import user_logged_in
from igaming.models import Casino_Settings
from account.models import Wallet

@receiver(pre_save, sender=Wallet)
def wallet_save_pre(sender, instance, **kwargs):
    """ Signal used for deposit bonus """

    if instance.currency != Wallet.BNS:
        try:
            settings = Casino_Settings.objects.get()
            wallet = Wallet.objects.get(id=instance.id)
            if wallet.cash + settings.login_bonus < instance.cash:

                user = User.objects.get(id=instance.user_id)
                new_bonus_wallet = Wallet.objects.create(user=user,
                                                         currency=Wallet.BNS)
                new_bonus_wallet.cash = settings.deposit_bonus
                new_bonus_wallet.wager_req = settings.wager_req

                new_bonus_wallet.save()

        except Wallet.DoesNotExist:
            pass

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Wallet.objects.create(user=instance, currency=Wallet.EUR)

@receiver(user_logged_in, sender=User)
def user_logs_in(sender, user, request, **kwargs):
    """ Signal used for login bonus """
    
    obj = Wallet.objects.get(user_id=user.id, currency=Wallet.EUR)
    settings = Casino_Settings.objects.get()
    obj.cash += settings.login_bonus
    obj.save()
