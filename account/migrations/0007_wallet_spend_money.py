# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-25 22:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0006_auto_20170525_2205'),
    ]

    operations = [
        migrations.AddField(
            model_name='wallet',
            name='spend_money',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
